��    M      �  g   �      �     �     �     �     �     �  	   �  
   �     �           	          !     2  
   >     I     P     \     i     q          �     �     �     �     �     �     �     �     �                 	   "  
   ,     7     <  	   T     ^     n  
   s  	   ~     �     �     �  
   �     �     �     �  
   �      	     	     	     )	     0	     B	     O	     `	     v	     �	     �	  !   �	     �	     �	     �	     
     
     -
     M
     _
     |
     �
     �
     �
     �
     �
     �
  m  �
     f     l     }     �     �     �     �     �            	   %     /     D     T     b     k     }     �     �  	   �     �     �     �     �                    0     8     U     ]     p     u     �     �     �  
   �     �  	   �     �     �     �       !   *     L     d  	   t     ~     �     �     �     �     �     �     �     �          )     =     X  &   l  
   �     �     �     �     �  &   �       #   0     T     \     q     �     �     �     �     8          !   C   7             H       *       K      I              -   5   D              "   J       ,   .       #       2   ?   M   $   +   3   @   
   =          )      (                  <                B   :   0   /   %   9   E               L      '                  6      >   ;   1   A         	          4   F                                          &   G           About About Title About item {#} Add Another About item Add Another Procedure Add Image Add Images Add New Testimonial Add icon Add testimonial Address All Testimonials Button Text Button URL Clinic Clinic Text Clinic Title Contact Contact Title Content Content Subtitle Content Text Content Title Description Download Edit Testimonial Featured Image File Filter Testimonials list Gallery Gallery images Hero Hero Text Hero Title Icon Insert into testimonial Know more New Testimonial News News Title Not found Not found in Trash Number of news Number of news to show Open hours Parent Testimonial: Phones Procedure {#} Procedures Procedures Title Profile Profile picture Remove Remove About item Remove Image Remove Procedure Remove featured image Search Testimonial Set featured image Show Testimonials Show Testimonials section on page Testimonial Testimonial Archives Testimonial Attributes Testimonials Testimonials categories Testimonials categories to show Testimonials list Testimonials list navigation Title Update Testimonial Uploaded to this testimonial Use as featured image View Testimonial View Testimonials testimonials Project-Id-Version: 
PO-Revision-Date: 2019-02-06 16:24-0200
Last-Translator: 
Language-Team: 
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.6
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: __
X-Poedit-SearchPath-0: drdenisvincenzicombr.php
 Sobre Título do Sobre Item do Sobre {#} Adicionar Novo Item do Sobre Adicionar Novo Procedimento Adicionar imagem Adicionar imagens Adicionar Novo Depoimento Adicionar ícone Adicionar novo Endereço Todos os Depoimentos Texto do botão URL do botão Clínica Texto de Clínica Título de Clínica Contato Título do Contato Conteúdo Subtítulo do Conteúdo Texto do Conteúdo Título do Conteúdo Descrição Download Editar Depoimento Imagem em destaque Arquivo Filtrar lista de Depoimentos Galeria Imagens da Galeria Hero Texto do Hero Título do Hero Ícone Inserir no depoimento Saiba mais Novo Depoimento Novidades Título de Notícias Não encontrado Não encontrado na lixeira Número de notícias Número de notícias para mostrar Horário de atendimento Depoimento Pai: Telefones Procedimento {#} Procedimentos Título de Procedimentos Perfil Foto do Perfil Remover Remover Item do Sobre Remover imagem Remover Procedimento Remover imagem em destaque Procurar Depoimento Definir imagem em destaque Mostrar Depoimentos Mostrar seção Depoimentos na página Depoimento Arquivo de Depoimentos Atributos do Depoimento Depoimentos Categorias dos Depoimentos Categorias dos depoimentos para exibir Lista de Depoimentos Navegação na lista de Depoimentos Título Atualizar Depoimento Carregado para este depoimento Usar como imagem destacada Ver Depoimento Ver Depoimentos depoimentos 