<?php
/**
 * Plugin Name: Dr. Denis Vincenzi - mu-plugin
 * Plugin URI: https://drdenisvincenzi.com.br
 * Description: Customizations for drdenisvincenzi.com.br site
 * Author: Nextcom
 * Version: 1.0.0
 * Author URI: https://nextcom.digital/
 * Text Domain: drdenisvincenzi
 */

// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}

/**
 * Load Translation
 *
 * @return void
 */
add_action(
    'plugins_loaded', function () {
        load_muplugin_textdomain('drdenisvincenzi', basename(dirname(__FILE__)) . '/languages');
    }
);

/**
 * Hide editor from all pages
 */
add_action(
    'admin_init', function () {
        remove_post_type_support('page', 'editor');
    }
);

/*********************************************************************************** 
 * Callback Functions 
 **********************************************************************************/
/**
 * Metabox for Page Slug
 * @author Tom Morton
 * @link https://github.com/CMB2/CMB2/wiki/Adding-your-own-show_on-filters
 *
 * @param bool $display
 * @param array $meta_box
 * @return bool display metabox
 */
function be_metabox_show_on_slug($display, $meta_box) 
{
    if (!isset($meta_box['show_on']['key'], $meta_box['show_on']['value'])) {
        return $display;
    }

    if ('slug' !== $meta_box['show_on']['key']) {
        return $display;
    }

    $post_id = 0;

    // If we're showing it based on ID, get the current ID
    if (isset($_GET['post'])) {
        $post_id = $_GET['post'];
    } elseif (isset($_POST['post_ID'])) {
        $post_id = $_POST['post_ID'];
    }

    if (!$post_id) {
        return $display;
    }

    $slug = get_post($post_id)->post_name;

    // See if there's a match
    return in_array($slug, (array) $meta_box['show_on']['value']);
}
add_filter('cmb2_show_on', 'be_metabox_show_on_slug', 10, 2);

 /**
  * Gets a number of terms and displays them as options
  * 
  * @param CMB2_Field $field 
  * 
  * @return array An array of options that matches the CMB2 options array
  */
function drdenisvincenzi_getTermOptions($field) 
{
    $args = $field->args('get_terms_args');
    $args = is_array($args) ? $args : array();

    $args = wp_parse_args($args, array('taxonomy' => 'category'));

    $taxonomy = $args['taxonomy'];

    $terms = (array) cmb2_utils()->wp_at_least('4.5.0')
        ? get_terms($args)
        : get_terms($taxonomy, $args);

    // Initate an empty array
    $term_options = array();
    if (!empty($terms) ) {
        foreach ($terms as $term) {
            $term_options[ $term->term_id ] = $term->name;
        }
    }

    return $term_options;
}

/************************************************************************************
 * Custom Post Types
 ***********************************************************************************/

/**
 * Testimonials
 */
add_action(
    'init', function () {
        $labels = array(
            'name'                  => __('Testimonials', 'drdenisvincenzi'),
            'singular_name'         => __('Testimonial', 'drdenisvincenzi'),
            'menu_name'             => __('Testimonials', 'drdenisvincenzi'),
            'name_admin_bar'        => __('Testimonial', 'drdenisvincenzi'),
            'archives'              => __('Testimonial Archives', 'drdenisvincenzi'),
            'attributes'            => __('Testimonial Attributes', 'drdenisvincenzi'),
            'parent_item_colon'     => __('Parent Testimonial:', 'drdenisvincenzi'),
            'all_items'             => __('All Testimonials', 'drdenisvincenzi'),
            'add_new_item'          => __('Add New Testimonial', 'drdenisvincenzi'),
            'add_new'               => __('Add testimonial', 'drdenisvincenzi'),
            'new_item'              => __('New Testimonial', 'drdenisvincenzi'),
            'edit_item'             => __('Edit Testimonial', 'drdenisvincenzi'),
            'update_item'           => __('Update Testimonial', 'drdenisvincenzi'),
            'view_item'             => __('View Testimonial', 'drdenisvincenzi'),
            'view_items'            => __('View Testimonials', 'drdenisvincenzi'),
            'search_items'          => __('Search Testimonial', 'drdenisvincenzi'),
            'not_found'             => __('Not found', 'drdenisvincenzi'),
            'not_found_in_trash'    => __('Not found in Trash', 'drdenisvincenzi'),
            'featured_image'        => __('Featured Image', 'drdenisvincenzi'),
            'set_featured_image'    => __('Set featured image', 'drdenisvincenzi'),
            'remove_featured_image' => __('Remove featured image', 'drdenisvincenzi'),
            'use_featured_image'    => __('Use as featured image', 'drdenisvincenzi'),
            'insert_into_item'      => __('Insert into testimonial', 'drdenisvincenzi'),
            'uploaded_to_this_item' => __('Uploaded to this testimonial', 'drdenisvincenzi'),
            'items_list'            => __('Testimonials list', 'drdenisvincenzi'),
            'items_list_navigation' => __('Testimonials list navigation', 'drdenisvincenzi'),
            'filter_items_list'     => __('Filter Testimonials list', 'drdenisvincenzi'),
        );
    
        $rewrite = array(
            'slug'                  => __('testimonials', 'drdenisvincenzi'),
            'with_front'            => true,
            'pages'                 => true,
            'feeds'                 => true,
        );
        
        $args = array(
            'label'                 => __('Testimonial', 'drdenisvincenzi'),
            'description'           => __('Testimonials', 'drdenisvincenzi'),
            'labels'                => $labels,
            'supports'              => array('title', 'revisions', 'page-attributes'),
            'taxonomies'            => array('category'),
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 7,
            'menu_icon'             => 'data:image/svg+xml;base64,'. base64_encode('<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path fill="black" d="M12 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm0 2c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z"/></svg>'),
            'show_in_admin_bar'     => false,
            'show_in_nav_menus'     => false,
            'can_export'            => true,
            'has_archive'           => false,
            'exclude_from_search'   => true,
            'publicly_queryable'    => false,
            'rewrite'               => $rewrite,
            'capability_type'       => 'page',
        );
        register_post_type('ddv_testimonials', $args); 
    }, 0
);

/**
 * Testimonials Custom Fields
 */
add_action(
    'cmb2_admin_init', function () {
        // Start with an underscore to hide fields from custom fields list
        $prefix = '_drdenisvincenzicombr_testimonial_';
        
        /**
        * Initiate the metabox
        */
        $cmb_testimonial = new_cmb2_box(
            array(
                'id'            => 'testimonials_id',
                'title'         => __('Testimonial', 'drdenisvincenzi'),
                'object_types'  => array('ddv_testimonials'), // Post type
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
                // 'cmb_styles' => false, // false to disable the CMB stylesheet
                // 'closed'     => true, // Keep the metabox closed by default
            )
        );

        //Profile picture
        $cmb_testimonial->add_field(
            array(
            'name'        => __('Profile picture', 'drdenisvincenzi'),
            'description' => '',
            'id'          => $prefix . 'profile_picture',
            'type'        => 'file',
            // Optional:
            'options' => array(
                    'url' => false, // Hide the text input for the url
            ),
            'text'    => array(
                    'add_upload_file_text' => __('Add Image', 'drdenisvincenzi'), // Change upload button text. Default: "Add or Upload File"
            ),
            // query_args are passed to wp.media's library query.
            'query_args' => array(
                    //'type' => 'image/jpeg', // Make library only display PDFs.
                    // Or only allow gif, jpg, or png images
                    'type' => array(
                    // 	'image/gif',
                            'image/jpeg',
                            'image/png',
                ),
            ),
            'preview_size' => array(180, 180) //'large', // Image size to use when previewing in the admin.
            )
        );

        //Profile
        $cmb_testimonial->add_field( 
            array(
                'name'       => __('Profile', 'drdenisvincenzi'),
                'desc'       => '',
                'id'         => $prefix . 'profile',
                'type'       => 'text',
            )
        );

        //Testimonial
        $cmb_testimonial->add_field( 
            array(
                'name'       => __('Testimonial', 'drdenisvincenzi'),
                'desc'       => '',
                'id'         => $prefix . 'text',
                'type'       => 'textarea_code',
            )
        );
    }
);

/*********************************************************************************** 
 * Pages Custom Fields
 * ********************************************************************************/

/**
 * Front-page 
 */
add_action(
    'cmb2_admin_init', function () {
        // Start with an underscore to hide fields from custom fields list
        $prefix = '_drdenisvincenzicombr_frontpage_';

        /**
         * Hero
         */
        $cmb_hero = new_cmb2_box(
            array(
                'id'            => 'frontpage_hero_id',
                'title'         => __('Hero', 'drdenisvincenzi'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'front-page'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Featured Image
        $cmb_hero->add_field(
            array (
                'name'        => __('Featured Image', 'drdenisvincenzi'),
                'description' => '',
                'id'          => $prefix . 'hero_featured_image',
                'type'        => 'file',
                // Optional:
                'options' => array(
                        'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                        'add_upload_file_text' => __('Add Image', 'drdenisvincenzi'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                        //'type' => 'image/jpeg', // Make library only display PDFs.
                        // Or only allow gif, jpg, or png images
                        'type' => array(
                        // 	'image/gif',
                            'image/jpeg',
                            'image/png',
                    ),
                ),
                'preview_size' => array(320, 240) //3:2 75%
            )
        );

        //Hero Title
        $cmb_hero->add_field( 
            array(
                'name'       => __('Hero Title', 'drdenisvincenzi'),
                'desc'       => '',
                'id'         => $prefix . 'hero_title',
                'type'       => 'textarea_code',
            )
        );

        //Hero Text
        $cmb_hero->add_field( 
            array(
                'name'       => __('Hero Text', 'drdenisvincenzi'),
                'desc'       => '',
                'id'         => $prefix . 'hero_text',
                'type'       => 'textarea_code',
            )
        );

        //Hero Button Text
        $cmb_hero->add_field( 
            array(
                'name'       => __('Button Text', 'drdenisvincenzi'),
                'desc'       => '',
                'default'    => __('Know more', 'drdenisvincenzi'),
                'id'         => $prefix . 'hero_btn_text',
                'type'       => 'text_small',
            )
        );

        //Hero Button URL
        $cmb_hero->add_field( 
            array(
                'name'       => __('Button URL', 'drdenisvincenzi'),
                'desc'       => '',
                'id'         => $prefix . 'hero_btn_url',
                'type'       => 'text_url',
            )
        );

        /**
         * Clinic
         */
        $cmb_clinic = new_cmb2_box(
            array(
                'id'            => 'frontpage_clinic_id',
                'title'         => __('Clinic', 'drdenisvincenzi'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'front-page'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Featured Image
        $cmb_clinic->add_field(
            array (
                'name'        => __('Featured Image', 'drdenisvincenzi'),
                'description' => '',
                'id'          => $prefix . 'clinic_featured_image',
                'type'        => 'file',
                // Optional:
                'options' => array(
                        'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                        'add_upload_file_text' => __('Add Image', 'drdenisvincenzi'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                        //'type' => 'image/jpeg', // Make library only display PDFs.
                        // Or only allow gif, jpg, or png images
                        'type' => array(
                        // 	'image/gif',
                            'image/jpeg',
                            'image/png',
                    ),
                ),
                'preview_size' => array(320, 240) //3:2 75%
            )
        );

        //Clinic Title
        $cmb_clinic->add_field( 
            array(
                'name'       => __('Clinic Title', 'drdenisvincenzi'),
                'desc'       => '',
                'id'         => $prefix . 'clinic_title',
                'type'       => 'textarea_code',
            )
        );

        //Clinic Text
        $cmb_clinic->add_field( 
            array(
                'name'       => __('Clinic Text', 'drdenisvincenzi'),
                'desc'       => '',
                'id'         => $prefix . 'clinic_text',
                'type'       => 'textarea_code',
            )
        );

        //Clinic Button Text
        $cmb_clinic->add_field( 
            array(
                'name'       => __('Button Text', 'drdenisvincenzi'),
                'desc'       => '',
                'default'    => __('Know more', 'drdenisvincenzi'),
                'id'         => $prefix . 'clinic_btn_text',
                'type'       => 'text_small',
            )
        );

        //Clinic Button URL
        $cmb_clinic->add_field( 
            array(
                'name'       => __('Button URL', 'drdenisvincenzi'),
                'desc'       => '',
                'id'         => $prefix . 'clinic_btn_url',
                'type'       => 'text_url',
            )
        );

        /**
         * About
         */
        $cmb_about = new_cmb2_box(
            array(
                'id'            => 'frontpage_about_id',
                'title'         => __('About', 'drdenisvincenzi'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'front-page'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //About Title
        $cmb_about->add_field( 
            array(
                'name'       => __('About Title', 'drdenisvincenzi'),
                'desc'       => '',
                'id'         => $prefix . 'about_title',
                'type'       => 'textarea_code',
            )
        );

        //About Group
        $about_id = $cmb_about->add_field( 
            array(
                'id'          => $prefix.'about_items',
                'type'        => 'group',
                'description' => '', 
                // 'repeatable'  => false, // use false if you want non-repeatable group
                'options'     => array(
                    'group_title'   => __('About item {#}', 'drdenisvincenzi'), // since version 1.1.4, {#} gets replaced by row number
                    'add_button'    => __('Add Another About item', 'drdenisvincenzi'),
                    'remove_button' => __('Remove About item', 'drdenisvincenzi'),
                    'sortable'      => true, // beta
                ),
            )
        );

        //About Item Icon
        $cmb_about->add_group_field(
            $about_id, array(
                'name'    => __('Icon', 'drdenisvincenzi'),
                'desc'    => "",
                'id'      => 'icon',
                'type'    => 'file',
                // Optional:
                'options' => array(
                        'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                        'add_upload_file_text' => __('Add icon', 'drdenisvincenzi'), 
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                        //'type' => 'image/jpeg', // Make library only display PDFs.
                        // Or only allow gif, jpg, or png images
                        'type' => array(
                        // 	'image/gif',
                                'image/jpeg',
                                'image/png',
                                'image/svg+xml',
                        ),
                ),
                'preview_size' => array(150, 150) //'large', // Image size to use when previewing in the admin.
            ) 
        );

        //About Item Title
        $cmb_about->add_group_field( 
            $about_id, array(
                'name' => __('Title', 'drdenisvincenzi'),
                'id'   => 'title',
                'type' => 'textarea_code',
            ) 
        );

        //About Item Description
        $cmb_about->add_group_field( 
            $about_id, array(
            'name' => __('Description', 'drdenisvincenzi'),
            'id'   => 'desc',
            'description' => '',
            'type' => 'textarea_code',
            ) 
        );

        /**
         * Procedures
         */
        $cmb_procedures = new_cmb2_box(
            array(
                'id'            => 'frontpage_procedures_id',
                'title'         => __('Procedures', 'drdenisvincenzi'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'front-page'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Procedures Title
        $cmb_procedures->add_field( 
            array(
                'name'       => __('Procedures Title', 'drdenisvincenzi'),
                'desc'       => '',
                'id'         => $prefix . 'procedures_title',
                'type'       => 'textarea_code',
            )
        );

        //Procedures Group
        $procedure_id = $cmb_procedures->add_field( 
            array(
                'id'          => $prefix.'procedures_items',
                'type'        => 'group',
                'description' => '', 
                // 'repeatable'  => false, // use false if you want non-repeatable group
                'options'     => array(
                    'group_title'   => __('Procedure {#}', 'drdenisvincenzi'), // since version 1.1.4, {#} gets replaced by row number
                    'add_button'    => __('Add Another Procedure', 'drdenisvincenzi'),
                    'remove_button' => __('Remove Procedure', 'drdenisvincenzi'),
                    'sortable'      => true, // beta
                ),
            )
        );

        //Procedure Item Featured Image
        $cmb_procedures->add_group_field(
            $procedure_id, array (
                'name'        => __('Featured Image', 'drdenisvincenzi'),
                'description' => '',
                'id'          => 'featured_image',
                'type'        => 'file',
                // Optional:
                'options' => array(
                        'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                        'add_upload_file_text' => __('Add Image', 'drdenisvincenzi'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                        //'type' => 'image/jpeg', // Make library only display PDFs.
                        // Or only allow gif, jpg, or png images
                        'type' => array(
                        // 	'image/gif',
                            'image/jpeg',
                            'image/png',
                    ),
                ),
                'preview_size' => array(320, 240) //3:2 75%
            )
        );

        //Procedure Item Title
        $cmb_procedures->add_group_field( 
            $procedure_id, array(
                'name' => __('Title', 'drdenisvincenzi'),
                'id'   => 'title',
                'type' => 'textarea_code',
            ) 
        );

        //Procedure Item Description
        $cmb_procedures->add_group_field( 
            $procedure_id, array(
            'name' => __('Description', 'drdenisvincenzi'),
            'id'   => 'desc',
            'description' => '',
            'type' => 'textarea_code',
            ) 
        );

        //Clinic Button Text
        $cmb_procedures->add_group_field( 
            $procedure_id, array(
                'name'       => __('Button Text', 'drdenisvincenzi'),
                'desc'       => '',
                'default'    => __('Know more', 'drdenisvincenzi'),
                'id'         => 'btn_text',
                'type'       => 'text_small',
            )
        );

        //Clinic Button URL
        $cmb_procedures->add_group_field( 
            $procedure_id, array(
                'name'       => __('Button URL', 'drdenisvincenzi'),
                'desc'       => '',
                'id'         => 'btn_url',
                'type'       => 'text_url',
            )
        );


        /**
         * News
         */
        $cmb_news = new_cmb2_box(
            array(
                'id'            => 'frontpage_news_id',
                'title'         => __('News', 'drdenisvincenzi'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'front-page'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //News Title
        $cmb_news->add_field( 
            array(
                'name'       => __('News Title', 'drdenisvincenzi'),
                'desc'       => '',
                'id'         => $prefix . 'news_title',
                'type'       => 'textarea_code',
            )
        );

        //Number of News 
        $cmb_news->add_field( 
            array(
                'name'       => __('Number of news', 'drdenisvincenzi'),
                'desc'       => __('Number of news to show', 'drdenisvincenzi'),
                'id'         => $prefix . 'news_number',
                'type'       => 'text',
                'attributes' => array(
                    'type' => 'number',
                    'min' => '3',
                    'max' => '9',
                    'step' => '3',
                ),
            )
        );

        /**
         * Contact
         */
        $cmb_contact = new_cmb2_box(
            array(
                'id'            => 'frontpage_contact_id',
                'title'         => __('Contact', 'drdenisvincenzi'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'front-page'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Contact Title
        $cmb_contact->add_field( 
            array(
                'name'       => __('Contact Title', 'drdenisvincenzi'),
                'desc'       => '',
                'id'         => $prefix . 'contact_title',
                'type'       => 'textarea_code',
            )
        );

        //Contact Address
        $cmb_contact->add_field( 
            array(
                'name'       => __('Address', 'drdenisvincenzi'),
                'desc'       => '',
                'id'         => $prefix . 'contact_address',
                'type'       => 'textarea_code',
            )
        );

        //Contact Phone
        $cmb_contact->add_field( 
            array(
                'name'       => __('Phones', 'drdenisvincenzi'),
                'desc'       => '',
                'id'         => $prefix . 'contact_phones',
                'type'       => 'textarea_code',
            )
        );

        //Contact Office hours
        $cmb_contact->add_field( 
            array(
                'name'       => __('Open hours', 'drdenisvincenzi'),
                'desc'       => '',
                'id'         => $prefix . 'contact_office_hours',
                'type'       => 'textarea_code',
            )
        );
    }
);

/**
 * Clinic
 */
add_action(
    'cmb2_admin_init', function () {
        // Start with an underscore to hide fields from custom fields list
        $prefix = '_drdenisvincenzicombr_clinic_';

        /**
         * Hero
         */
        $cmb_hero = new_cmb2_box(
            array(
                'id'            => 'clinic_hero_id',
                'title'         => __('Hero', 'drdenisvincenzi'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'clinica'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Hero Title
        $cmb_hero->add_field( 
            array(
                'name'       => __('Hero Title', 'drdenisvincenzi'),
                'desc'       => '',
                'id'         => $prefix . 'hero_title',
                'type'       => 'text',
            )
        );
    
        /**
         * Content
         */
        $cmb_content = new_cmb2_box(
            array(
                'id'            => 'clinic_content_id',
                'title'         => __('Content', 'drdenisvincenzi'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'clinica'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Content Title
        $cmb_content->add_field( 
            array(
                'name'       => __('Content Title', 'drdenisvincenzi'),
                'desc'       => '',
                'id'         => $prefix . 'content_title',
                'type'       => 'textarea_code',
            )
        );

        //Content Subtitle
        $cmb_content->add_field( 
            array(
                'name'       => __('Content Subtitle', 'drdenisvincenzi'),
                'desc'       => '',
                'id'         => $prefix . 'content_subtitle',
                'type'       => 'textarea_code',
            )
        );

        //Content Text
        $cmb_content->add_field( 
            array(
                'name'       => __('Content Text', 'drdenisvincenzi'),
                'desc'       => '',
                'id'         => $prefix . 'content_text',
                'type'       => 'textarea_code',
            )
        );
        
        /**
         * Gallery
         */
        $cmb_gallery = new_cmb2_box(
            array(
                'id'            => 'clinic_gallery_id',
                'title'         => __('Gallery', 'drdenisvincenzi'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'clinica'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Gallery Images
        $cmb_gallery->add_field(
            array(
                'name' => __('Gallery images', 'drdenisvincenzi'),
                'id'   =>  $prefix . 'gallery_images',
                'type' => 'file_list',
                // Optional:
                'options' => array(
                        'url' => false, // Hide the text input for the url
                ),
                'text' => array(
                    'add_upload_files_text' => __('Add Images', 'drdenisvincenzi'), // default: "Add or Upload Files"
                    'remove_image_text' => __('Remove Image', 'drdenisvincenzi'), // default: "Remove Image"
                    'file_text' => __('File', 'drdenisvincenzi'), // default: "File:"
                    'file_download_text' => __('Download', 'drdenisvincenzi'), // default: "Download"
                    'remove_text' => __('Remove', 'drdenisvincenzi'), // default: "Remove"
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                        //'type' => 'image/jpeg', // Make library only display PDFs.
                        // Or only allow gif, jpg, or png images
                        'type' => array(
                        // 	'image/gif',
                                'image/jpeg',
                                'image/png',
                        ),
                ),
                'preview_size' => array(320, 240) //'large', // Image size to use when previewing in the admin.
            )
        );

        /**
         * Testimonials
         */
        $cmb_testimonials = new_cmb2_box(
            array(
                'id'            => 'clinic_testimonials_id',
                'title'         => __('Testimonials', 'drdenisvincenzi'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'clinica'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Testimonials Show
        $cmb_testimonials->add_field( 
            array(
            'name' => __('Show Testimonials', 'drdenisvincenzi'),
            'desc' => __('Show Testimonials section on page', 'drdenisvincenzi'),
            'type' => 'checkbox',
            'id'   => $prefix . 'testimonials_show',
            )
        );

        //Testimonials Categories
        $cmb_testimonials->add_field( 
            array(
            'name'    => __('Testimonials categories', 'drdenisvincenzi'),
            'desc'    => __('Testimonials categories to show', 'drdenisvincenzi'),
            'id'      => $prefix . 'testimonials_categories',
            'type'    => 'multicheck',
            'options_cb'     => 'drdenisvincenzi_getTermOptions',
            // Same arguments you would pass to `get_terms`.
            'get_terms_args' => array(
                'post_type'  => 'ddv_testimonials',
                'taxonomy'   => 'category',
                'hide_empty' => false,
                ),
            )
        );
    }
);

/**
 * Procedure 
 */
add_action(
    'cmb2_admin_init', function () {
        // Start with an underscore to hide fields from custom fields list
        $prefix = '_drdenisvincenzicombr_procedure_';
    
        /**
         * Hero
         */
        $cmb_hero = new_cmb2_box(
            array(
                'id'            => 'procedure_hero_id',
                'title'         => __('Hero', 'drdenisvincenzi'),
                'object_types'  => array('page'), // post type
                'show_on'       =>  array( 'key' => 'page-template', 'value' => 'views/template-procedimento.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );
    
        //Hero Title
        $cmb_hero->add_field( 
            array(
                'name'       => __('Hero Title', 'drdenisvincenzi'),
                'desc'       => '',
                'id'         => $prefix . 'hero_title',
                'type'       => 'text',
            )
        );
    
        /**
         * Content
         */
        $cmb_content = new_cmb2_box(
            array(
                'id'            => 'procedure_content_id',
                'title'         => __('Content', 'drdenisvincenzi'),
                'object_types'  => array('page'), // post type
                'show_on'       =>  array( 'key' => 'page-template', 'value' => 'views/template-procedimento.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );
    
        //Content Title
        $cmb_content->add_field( 
            array(
                'name'       => __('Content Title', 'drdenisvincenzi'),
                'desc'       => '',
                'id'         => $prefix . 'content_title',
                'type'       => 'textarea_code',
            )
        );
    
        //Content Subtitle
        $cmb_content->add_field( 
            array(
                'name'       => __('Content Subtitle', 'drdenisvincenzi'),
                'desc'       => '',
                'id'         => $prefix . 'content_subtitle',
                'type'       => 'textarea_code',
            )
        );
    
        //Content Text
        $cmb_content->add_field( 
            array(
                'name'       => __('Content Text', 'drdenisvincenzi'),
                'desc'       => '',
                'id'         => $prefix . 'content_text',
                'type'       => 'textarea_code',
            )
        );
    
        /**
         * Procedures
         */
        $cmb_procedures = new_cmb2_box(
            array(
                'id'            => 'procedure_procedures_id',
                'title'         => __('Procedures', 'drdenisvincenzi'),
                'object_types'  => array('page'), // post type
                'show_on'       =>  array( 'key' => 'page-template', 'value' => 'views/template-procedimento.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Procedures Group
        $procedure_id = $cmb_procedures->add_field( 
            array(
                'id'          => $prefix.'procedures_items',
                'type'        => 'group',
                'description' => '', 
                // 'repeatable'  => false, // use false if you want non-repeatable group
                'options'     => array(
                    'group_title'   => __('Procedure {#}', 'drdenisvincenzi'), // since version 1.1.4, {#} gets replaced by row number
                    'add_button'    => __('Add Another Procedure', 'drdenisvincenzi'),
                    'remove_button' => __('Remove Procedure', 'drdenisvincenzi'),
                    'sortable'      => true, // beta
                ),
            )
        );

        //Procedure Item Featured Image
        $cmb_procedures->add_group_field(
            $procedure_id, array(
                'name'    => __('Featured Image', 'drdenisvincenzi'),
                'desc'    => "",
                'id'      => 'featured_image',
                'type'    => 'file',
                // Optional:
                'options' => array(
                        'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                        'add_upload_file_text' => __('Add image', 'drdenisvincenzi'), 
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                        //'type' => 'image/jpeg', // Make library only display PDFs.
                        // Or only allow gif, jpg, or png images
                        'type' => array(
                        // 	'image/gif',
                                'image/jpeg',
                                'image/png',
                                'image/svg+xml',
                        ),
                ),
                'preview_size' => array(320, 240) //'large', // Image size to use when previewing in the admin.
            ) 
        );

        //Procedure Item Title
        $cmb_procedures->add_group_field( 
            $procedure_id, array(
                'name' => __('Title', 'drdenisvincenzi'),
                'id'   => 'title',
                'type' => 'text',
            ) 
        );

        /**
         * Testimonials
         */
        $cmb_testimonials = new_cmb2_box(
            array(
                'id'            => 'procedure_testimonials_id',
                'title'         => __('Testimonials', 'drdenisvincenzi'),
                'object_types'  => array('page'), // post type
                'show_on'       =>  array( 'key' => 'page-template', 'value' => 'views/template-procedimento.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );
    
        //Testimonials Show
        $cmb_testimonials->add_field( 
            array(
            'name' => __('Show Testimonials', 'drdenisvincenzi'),
            'desc' => __('Show Testimonials section on page', 'drdenisvincenzi'),
            'type' => 'checkbox',
            'id'   => $prefix . 'testimonials_show',
            )
        );
    
        //Testimonials categories
        $cmb_testimonials->add_field( 
            array(
            'name'    => 'Testimonials Categories',
            'desc'    => 'Testimonials categories to show',
            'id'      => $prefix . 'testimonials_categories',
            'type'    => 'multicheck',
            'options_cb'     => 'drdenisvincenzi_getTermOptions',
            // Same arguments you would pass to `get_terms`.
            'get_terms_args' => array(
                'post_type'  => 'ddv_testimonials',
                'taxonomy'   => 'category',
                'hide_empty' => false,
                ),
            )
        );
    }
);

/**
 * Home (Blog)
 */
add_action(
    'cmb2_admin_init', function () {
        // Start with an underscore to hide fields from custom fields list
        $prefix = '_drdenisvincenzicombr_blog_';

        /******
         * Hero
         ******/
        $cmb_hero = new_cmb2_box(
            array(
                'id'            => 'home_hero_id',
                'title'         => __('Hero', 'drdenisvincenzi'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'blog'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Hero Title
        $cmb_hero->add_field( 
            array(
                'name'       => __('Hero Title', 'drdenisvincenzi'),
                'desc'       => '',
                'id'         => $prefix . 'hero_title',
                'type'       => 'text',
                'default'    => 'Blog',
            )
        );
    }
);

/**
 * Single Post (Blog)
 */
add_action(
    'cmb2_admin_init', function () {
        // Start with an underscore to hide fields from custom fields list
        $prefix = '_drdenisvincenzicombr_post_';

        /******
         * Hero
         ******/
        $cmb_hero = new_cmb2_box(
            array(
                'id'            => 'post_hero_id',
                'title'         => __('Hero', 'drdenisvincenzi'),
                'object_types'  => array('post'), // post type
                //'show_on'       =>  array('key' => 'page-template', 'value' => 'views/home.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Hero Title
        $cmb_hero->add_field( 
            array(
                'name'       => __('Hero Title', 'drdenisvincenzi'),
                'desc'       => '',
                'id'         => $prefix . 'hero_title',
                'type'       => 'text',
                'default'    => 'Blog',
            )
        );

        /**
         * Content
         */
        $cmb_content = new_cmb2_box(
            array(
                'id'            => 'post_content_id',
                'title'         => __('Content', 'drdenisvincenzi'),
                'object_types'  => array('post'), // post type
                //'show_on' => array('key' => 'slug', 'value' => 'clinica'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Content Subtitle
        $cmb_content->add_field( 
            array(
                'name'       => __('Content Subtitle', 'drdenisvincenzi'),
                'desc'       => '',
                'id'         => $prefix . 'content_subtitle',
                'type'       => 'textarea_code',
            )
        );
    }
);
?>